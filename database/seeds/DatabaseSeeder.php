<?php

use Illuminate\Database\Seeder;

use App\Semester;
use App\Department;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        Department::create([
        	'title' => 'agriculture'
        ]);
        Department::create([
        	'title' => 'forestry'
        ]);
        Department::create([
        	'title' => 'vetenary'
        ]);
        Semester::create([
            'title' => 'semester one'
        ]);
        Semester::create([
            'title' => 'semester two'
        ]);
        Semester::create([
            'title' => 'semester three'
        ]);
        Semester::create([
            'title' => 'semester four'
        ]);
        Semester::create([
            'title' => 'semester five'
        ]);
        Semester::create([
            'title' => 'semester six'
        ]);
        Semester::create([
            'title' => 'semester seven'
        ]);
        Semester::create([
            'title' => 'semester eight'
        ]);
    }
}
