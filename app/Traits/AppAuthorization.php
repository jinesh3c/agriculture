<?php
namespace App\Traits;

use App\Exceptions\AppKeyException;

trait AppAuthorization {

	public function authorizeToken($request) {
		$appKey = 'ede7353f371663de51accb482e11670b930da7d90b3c4703261f66255149347a5a97e';
		$app_key = $request->headers->get('X-APP-TOKEN');
		if((!isset($app_key)) || ($appKey != $app_key)){
			throw new AppKeyException();
		}
		return true;
	}
}