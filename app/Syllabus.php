<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Syllabus extends Model
{
    public function syllabus_file(){
    	return $this->hasMany('App\SyllabusFile');
    }
}
