<?php

namespace App\Http\Controllers\Api\v1;

use Auth;
use Validator;
use App\Contact;
use Illuminate\Http\Request;
use App\Traits\AppAuthorization;
use App\Http\Controllers\Controller;
use \Illuminate\Http\Response as Res;

class ContactController extends BaseController
{
    use AppAuthorization;
    public function __construct(Request $request) {
        $this->authorizeToken($request);
    }
    /**
        * @OA\post(
        *     path="/contact",
        *     tags={"contact"},
        *     description="send message",
        *     summary="send message to admin admins",
        *     security= {{"App_Key":"",}},
        *     @OA\RequestBody(
        *         description="conatct send message",
        *         required=true,
        *          @OA\MediaType(
        *             mediaType="multipart/form-data",
        *             @OA\Schema(
        *                 type="object",
        *                 @OA\Property(property="name",description="name",type="string",),
        *                 @OA\Property(property="email",description="email",type="string",),
        *                 @OA\Property(property="subject",description="subject",type="string",),
        *                 @OA\Property(property="message",description="message",type="string",),
        *             ),
        *           ),
        *     ),
        *     @OA\Response(
        *         response=200,
        *         description="message sent successfully",
        *         @OA\JsonContent(
        *             type="object",
        *         ),
        *         @OA\Link(
        *              link="index",
        *              operationId="message",
        *           ),
        *     ),
        *     @OA\Response(
        *          response="default",
        *          description="an ""unexpected"" error",
        *          @OA\JsonContent(
        *             type="object",
        *          ),
        *      ),
        * )
    */
    public function postContact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|string',
            'email'  => 'required',
            'message'  => 'required',
        ]);

        if($validator->fails()){
            $this->setStatusCode(Res::HTTP_UNPROCESSABLE_ENTITY);
            return $this->respondValidationError('Validation Error.', $validator->errors());
        }
        $data = new Contact;
        $data->name = $request->name;
        $data->email = $request->email;
        $data->subject = $request->subject;
        $data->message = $request->message;
        $data->save();
        $this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'contact post success');
    }
}
