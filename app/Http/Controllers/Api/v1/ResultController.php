<?php

namespace App\Http\Controllers\Api\v1;

use Auth;
use App\Result;
use Validator;
use Illuminate\Http\Request;
use App\Traits\AppAuthorization;
use App\Http\Controllers\Controller;
use \Illuminate\Http\Response as Res;

class ResultController extends BaseController
{
	use AppAuthorization;
    public function __construct(Request $request) {
        $this->authorizeToken($request);
    }
    /** 
        *   @OA\get(
        *     path="/result/filter",
        *     tags={"Search"},
        *     description="result filter",
        *     summary="result filter",
        *     security= {{"App_Key":"",}},
        *     @OA\Parameter(name="year", in="query", description="year",
        *          @OA\Schema(type="string",), 
        *      ),
        *     @OA\Parameter(name="offset", in="query", description="offset",
        *          @OA\Schema(type="string",), 
        *      ),
        *     @OA\Parameter(name="limit", in="query", description="limit",
        *          @OA\Schema(type="string",), 
        *      ),
        *     @OA\Response(response=200,description="note filtered successful",
        *         @OA\JsonContent(type="object",
        *         ),
        *         @OA\Link(
        *             link="SearchFilter",
        *             operationId="SearchFilter",
        *             parameters={
        *                   "year":"2019",
        *                   "offset":"1",
        *                   "limit":"10",
        *             },
        *          ),
        *     ),
        *     @OA\Response( response="default",description="unexpected error",
        *         @OA\JsonContent(type="object",
        *         ),
        *     ),
        * )
    */
    public function filter(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'year'  => 'required',
            'offset'  => 'required',
            'limit'  => 'required',
        ]);

        if($validator->fails()){
            $this->setStatusCode(Res::HTTP_UNPROCESSABLE_ENTITY);
            return $this->respondValidationError('Validation Error.', $validator->errors());
        }
    	$data = Result::where('year',$request->year)
    				->with('result_file')
    				->skip($request->offset*$request->limit)
    				->limit($request->limit)
    				->get();
    	if(count($data)<1) {
            $this->setStatusCode(Res::HTTP_NOT_FOUND);
            return $this->respondNotFound('No note matched your query');
        }
        $this->setStatusCode(Res::HTTP_OK);
        return $this->sendSuccessResponse($data, 'note list success');
    }
}
