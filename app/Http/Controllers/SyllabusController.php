<?php

namespace App\Http\Controllers;

use Storage;
use App\Syllabus;
use App\Department;
use App\SyllabusFile;
use Illuminate\Http\Request;

class SyllabusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $syllabi = Syllabus::orderBy('id','desc')->paginate(10);
        return view('admin.syllabi.index',compact('syllabi'));
    }
    public function getSyllabusFile(Request $request){
        $data = SyllabusFIle::where('syllabus_id',$request->id)->get();
        $response = array(
            'status' => 'success',
            'msg' => $data
        );
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.syllabi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'files' => 'required',
            'files.*' => 'mimes:doc,pdf,docx'
        ]);
        $data = new Syllabus;
        $data->title = $request->title;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/syllabus/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/syllabus/', $name);  
                $dataFile = new SyllabusFile;
                $dataFile->syllabus_id = $data->id;
                $dataFile->file_title = $title;
                $dataFile->file_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('syllabi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $syllabus = Syllabus::find($id);
        return view('admin.syllabi.edit',compact('syllabus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'files' => 'required',
            'files.*' => 'mimes:doc,pdf,docx'
        ]);
        $data = Syllabus::find($id);
        $data->title = $request->title;
        $data->save();

        if($request->hasfile('files'))
         {
            foreach($request->file('files') as $file)
            {
                $title=$file->getClientOriginalName();
                $name = '/syllabus/'.str_random(10).'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/syllabus/', $name);  
                $dataFile = new SyllabusFile;
                $dataFile->syllabus_id = $data->id;
                $dataFile->file_title = $title;
                $dataFile->file_name = $name;
                $dataFile->save();
            }
         }
        return redirect()->route('syllabi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $files = SyllabusFile::where('syllabus_id',$id)->get();
        foreach($files as $file){
            Storage::delete($file->file_name);
            $file->delete();
        }
        Syllabus::find($id)->delete();
        return redirect()->route('syllabi.index');
    }
    public function deleteSyllabusFile($id)
    {
        $file = SyllabusFile::find($id);
        Storage::delete($file->file_name);
        $file->delete();
        return redirect()->back();
    }
}
