<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
	public function department(){
    	return $this->belongsTo('App\Department');
    }
    public function semester(){
    	return $this->belongsTo('App\Semester');
    }
    public function note_file(){
    	return $this->hasMany('App\NoteFile');
    }
}
