<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{asset('adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{Auth::user()->name}}</p>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">s
      <li class="header">MAIN NAVIGATION</li>
      <li>
        <a href="{{route('department.index')}}">
          <i class="fa fa-building"></i> <span>Department</span>
        </a>
      </li>
      <li>
        <a href="{{route('notice.index')}}">
          <i class="fa fa-th"></i> <span>Notices</span>
        </a>
      </li>
      <li>
        <a href="{{route('note.index')}}">
          <i class="fa fa-th"></i> <span>Notes</span>
        </a>
      </li>
      <li>
        <a href="{{route('result.index')}}">
          <i class="fa fa-th"></i> <span>Result</span>
        </a>
      </li>
      <li>
        <a href="{{route('syllabi.index')}}">
          <i class="fa fa-th"></i> <span>Syllabus</span>
        </a>
      </li>
      <li>
        <a href="{{route('contact.index')}}">
          <i class="fa fa-th"></i> <span>Contact</span>
        </a>
      </li>
      <li>
        <a href="{{route('question.index')}}">
          <i class="fa fa-th"></i> <span>OldQuestion</span>
        </a>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>