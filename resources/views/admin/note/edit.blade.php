@extends('layouts.app')
@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Edit Note</h3>
              </div>
              <div class="box-body">
              <form  method="POST" action="{{route('note.update',$note->id)}}" enctype="multipart/form-data">
                   @csrf
                   @method('PUT')
                   <div class="form-group">
                      <label>Department:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <select name="department_id" class="form-control" required>
                            <option value="{{null}}">Select Department</option>
                            @foreach($departments as $department)
                            <option value="{{$department->id}}" @if($note->department_id==$department->id) selected @endif>{{$department->title}}</option>
                            @endforeach
                          </select>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Semester:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <select name="semester" class="form-control" required>
                            <option value="{{null}}">Select Semester</option>
                            <option value="{{'1'}}" @if($note->semester_id=='1') selected @endif>semester one</option>
                            <option value="{{'2'}}" @if($note->semester_id=='2') selected @endif>semester two</option>
                            <option value="{{'3'}}" @if($note->semester_id=='3') selected @endif>semester three</option>
                            <option value="{{'4'}}" @if($note->semester_id=='4') selected @endif>semester four</option>
                            <option value="{{'5'}}"@if($note->semester_id=='5') selected @endif>semester five</option>
                            <option value="{{'6'}}"@if($note->semester_id=='6') selected @endif>semester six</option>
                            <option value="{{'7'}}"@if($note->semester_id=='7') selected @endif>semester seven</option>
                            <option value="{{'8'}}"@if($note->semester_id=='8') selected @endif>semester eight</option>
                          </select>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Subject:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="subject" id="subject" placeholder="subject" value="{{$note->subject}}" required>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Notes:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="file" class="form-control" name="files[]" multiple="multiple">
                      </div>
                   </div>
                   <div id="file-action" class="row"></div>
                  </div>
                   <div class="form-group">
                     <div class="form-input">
                       <input type="submit" class="btn btn-xs btn-primary" value="SUBMIT">
                     </div>
                   </div>
                 </form>
              </div>
          </div>
      </div>
  </div>
</div>

@endSection
@section('scripts')
<script>

  $(document).ready(function(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var data = $('#subject').val();
    if(data){
      $.ajax({
          url: '{{route('ajaxGetNoteFile')}}',
          data:{
              _token: CSRF_TOKEN,
              id: <?php echo $note->id; ?>,
          },
          dataType: 'JSON',
          success:function(data){
              console.log(data.msg)
              var files = data.msg
              $('#file-action').html('');
              $.each(files, function(index, value){
                  $('#file-action').append('<div class="col-md-2">'+(value.title)+'<form method="post" action="/notefile/'+(value.id)+'/delete">@csrf @method("DELETE")<button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></form></div>');
                  // $('#file-action').append($('<option>',{value:value.id}).text(value.name));
                  console.log(value);
              });
          }
      })
    }
  })
</script>
@endsection