@extends('layouts.app')
@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Add Old Question</h3>
              </div>
              <div class="box-body">
              <form  method="POST" action="{{route('question.store')}}" enctype="multipart/form-data">
                   @csrf
                   <div class="form-group">
                      <label>Department:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <select name="department_id" class="form-control" required>
                            <option value="{{null}}">Select Department</option>
                            @foreach($departments as $department)
                            <option value="{{$department->id}}">{{$department->title}}</option>
                            @endforeach
                          </select>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Semester:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <select name="semester" class="form-control" required>
                            <option value="{{null}}">Select Semester</option>
                            <option value="{{'1'}}">semester one</option>
                            <option value="{{'2'}}">semester two</option>
                            <option value="{{'3'}}">semester three</option>
                            <option value="{{'4'}}">semester four</option>
                            <option value="{{'5'}}">semester five</option>
                            <option value="{{'6'}}">semester six</option>
                            <option value="{{'7'}}">semester seven</option>
                            <option value="{{'8'}}">semester eight</option>
                          </select>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Subject:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="subject" placeholder="subject" required>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Questions:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="file" class="form-control" name="files[]" required multiple="multiple">
                      </div>
                   </div>
                  </div>
                   <div class="form-group">
                     <div class="form-input">
                       <input type="submit" class="btn btn-xs btn-primary" value="SUBMIT">
                     </div>
                   </div>
                 </form>
              </div>
          </div>
      </div>
  </div>
</div>
@endSection
@section('scripts')
<script>
  
</script>
@endsection