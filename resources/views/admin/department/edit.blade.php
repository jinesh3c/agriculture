@extends('layouts.app')
@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Add Department</h3>
              </div>
              <div class="box-body">
              <form  method="POST" action="{{route('department.update',$department->id)}}" enctype="multipart/form-data">
                   @csrf
                   @method('PUT')
                   <div class="form-group">
                      <label>Department Title:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input name="department" type="text" class="form-control @error('department') is-invalid @enderror" value="{{ $department->title }}" placeholder="department title" required>
                      </div>
                      @error('department')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                      @enderror
                   </div>
                  </div>
                   <div class="form-group">
                     <div class="form-input">
                       <input type="submit" class="btn btn-xs btn-primary" value="SUBMIT">
                     </div>
                   </div>
                 </form>
              </div>
          </div>
      </div>
  </div>
</div>
@endSection
@section('scripts')
<script>
  
</script>
@endsection