@extends('layouts.app')
@section('styles')
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection
@section('content')
<script src="{{asset('adminlte/bower_components/ckeditor/ckeditor.js')}}"></script>
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-10">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Ticket</h3>
                <div class="pull-right">
                <a href="{{route('department.create')}}" class="btn btn-xs btn-primary">Add Department</a>
                </div>
              </div>
              <div class="box-body">
                <table id="dataTable" class="table table-bordered table-hover">
                  <thead>
                   <tr>
                    <th>SN</th>
                    <th>title</th>
                    <th>Action</th>
                  </tr>
                 </thead>
                 <tbody>
                 @foreach($departments as $k=>$department)
                 <tr>
                   <td>{{$k+1}}</td>
                   <td>{{$department->title}}</td>
                   <td>
                     <form action="{{route('department.destroy',$department->id)}}" method="post">
                     <a href="{{route('department.edit',$department->id)}}" class="btn btn-xs btn-warning">EDIT</a>
                      @csrf 
                      @method('DELETE')
                      <input type="submit" class="btn btn-xs btn-danger" value="DELETE">
                     </form>
                   </td>
                 </tr>
                 @endforeach
                 </tbody>
               </table>
               <div class="text-center">
                 {{ $departments->links() }}
               </div>  
            </div>
        </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg" id="assign-modal-default">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <label for="">Ticket Issue:</label>
            <div class="well" id="ticketDescription">
             
            </div>
            <div class="form-group">
                <label>Department:</label>
                <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-chevron-down"></i>
                    </div>
                    <select name="department" id="department" class="form-control select2 @error('department') is-invalid @enderror" style="width: 100%;">
                      <option value="0">Select Department</option>
                      @foreach($departments as $department)
                      <option value="{{$department->id}}">{{$department->title}}</option>
                      @endforeach
                    </select>
                  </div>
            </div>
            <div id="level" class="form-group">
                <label>Level:</label>
                <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-chevron-down"></i>
                    </div>
                    <select name="level" id="userLevel" class="form-control select2 @error('level') is-invalid @enderror" style="width: 100%;" value={{old('level')}}>
                        <option value="0">Select level</option>
                        <option value="L1">level One</option>
                        <option value="L2">level Two</option>
                        <option value="L3">level Three</option>
                      </select>
                  </div>
            </div>
            <div id="users" class="form-group">
                <label>Assign Level User:</label>
                <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-chevron-down"></i>
                    </div>
                    <select name="assigned_user" id="user" class="form-control select2 @error('level') is-invalid @enderror" style="width: 100%;" value={{old('level')}}>
                        <option value="{{null}}">Select user</option>
                      </select>
                  </div>
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
    </div>
@endSection
@section('scripts')
<script src="{{asset('adminlte/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
//data table
    $('#dataTable').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : false,
      'autoWidth'   : false
    });
</script>
@endsection