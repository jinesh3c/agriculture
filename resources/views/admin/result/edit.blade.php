@extends('layouts.app')
@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Edit Note</h3>
              </div>
              <div class="box-body">
              <form  method="POST" action="{{route('result.update',$result->id)}}" enctype="multipart/form-data">
                   @csrf
                   @method('PUT')
                   <div class="form-group">
                      <label>Title:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="title" id="title" placeholder="title" value="{{$result->title}}" required>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Year:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="year" id="year" placeholder="year" value="{{$result->year}}" required>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Notes:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="file" class="form-control" name="files[]" multiple="multiple">
                      </div>
                   </div>
                   <div id="file-action" class="row"></div>
                  </div>
                   <div class="form-group">
                     <div class="form-input">
                       <input type="submit" class="btn btn-xs btn-primary" value="SUBMIT">
                     </div>
                   </div>
                 </form>
              </div>
          </div>
      </div>
  </div>
</div>

@endSection
@section('scripts')
<script>

  $(document).ready(function(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var data = $('#title').val();
    if(data){
      $.ajax({
          url: '{{route('ajaxGetResultFile')}}',
          data:{
              _token: CSRF_TOKEN,
              id: <?php echo $result->id; ?>,
          },
          dataType: 'JSON',
          success:function(data){
              console.log(data.msg)
              var files = data.msg
              $('#file-action').html('');
              $.each(files, function(index, value){
                  $('#file-action').append('<div class="col-md-2">'+(value.title)+'<form method="post" action="/resultfile/'+(value.id)+'/delete">@csrf @method("DELETE")<button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></form></div>');
                  // $('#file-action').append($('<option>',{value:value.id}).text(value.name));
                  console.log(value);
              });
          }
      })
    }
  })
</script>
@endsection