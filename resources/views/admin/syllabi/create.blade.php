@extends('layouts.app')
@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Add Syllabus</h3>
              </div>
              <div class="box-body">
              <form  method="POST" action="{{route('syllabi.store')}}" enctype="multipart/form-data">
                   @csrf
                   <div class="form-group">
                      <label>Title:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="title" placeholder="title" required>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Notes:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="file" class="form-control" name="files[]" required multiple="multiple">
                      </div>
                   </div>
                  </div>
                   <div class="form-group">
                     <div class="form-input">
                       <input type="submit" class="btn btn-xs btn-primary" value="SUBMIT">
                     </div>
                   </div>
                 </form>
              </div>
          </div>
      </div>
  </div>
</div>
@endSection
@section('scripts')
<script>
  
</script>
@endsection