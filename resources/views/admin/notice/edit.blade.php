@extends('layouts.app')
@section('styles')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="box">
              <div class="box-header">
                <h3 class="box-title">Edit Notice</h3>
              </div>
              <div class="box-body">
              <form  method="POST" action="{{route('notice.update',$notice->id)}}" enctype="multipart/form-data">
                   @csrf
                   @method('PUT')
                   <div class="form-group">
                      <label>Title:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="text" class="form-control" name="title" id="subject" placeholder="title" value="{{$notice->title}}" required>
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Notices:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="file" class="form-control" name="files[]" multiple="multiple">
                      </div>
                   </div>
                   <div class="form-group">
                      <label>Images:</label>
                      <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-building"></i>
                          </div>
                          <input type="file" class="form-control" name="images[]" multiple="multiple">
                      </div>
                   </div>
                   <div id="file-action" class="row"></div>
                  </div>
                   <div class="form-group">
                     <div class="form-input">
                       <input type="submit" class="btn btn-xs btn-primary" value="SUBMIT">
                     </div>
                   </div>
                 </form>
              </div>
          </div>
      </div>
  </div>
</div>

@endSection
@section('scripts')
<script>

  $(document).ready(function(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var data = $('#subject').val();
    if(data){
      $.ajax({
          url: '{{route('ajaxGetNoticeFile')}}',
          data:{
              _token: CSRF_TOKEN,
              id: <?php echo $notice->id; ?>,
          },
          dataType: 'JSON',
          success:function(data){
              console.log(data.msg)
              var files = data.msg
              $('#file-action').html('');
              $.each(files, function(index, value){
                  $('#file-action').append('<div class="col-md-2">'+(value.file_title)+'<form method="post" action="/noticefile/'+(value.id)+'/delete">@csrf @method("DELETE")<button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></form></div>');
                  // $('#file-action').append($('<option>',{value:value.id}).text(value.name));
                  console.log(value);
              });
          }
      })
    }
    
  })
</script>
@endsection