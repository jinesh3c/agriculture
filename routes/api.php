<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware'=>['api'],'prefix'=>'/v1'], function(){
	Route::post('/contact', 'Api\v1\ContactController@postContact');	
	Route::get('/note/filter', 'Api\v1\NoteController@filter');
	Route::get('/oldquestion/filter', 'Api\v1\OldQuestionController@filter');
	Route::get('/result/filter', 'Api\v1\ResultController@filter');
	Route::get('/syllabus', 'Api\v1\SyllabusController@filter');
	Route::get('/notice', 'Api\v1\NoticeController@getNotice');	
});
