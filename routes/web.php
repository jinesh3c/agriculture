<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' =>['auth']], function(){
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/notice', 'NoticeController');
Route::get('/ajaxGetNoticeFile', 'NoticeController@getNoticeFile')->name('ajaxGetNoticeFile');
Route::delete('/noticefile/{id}/delete', 'NoticeController@deleteNoticeFile')->name('deleteNoticeFile');

Route::resource('/contact', 'ContactController');

Route::resource('/note', 'NoteController');
Route::get('/ajaxGetNoteFile', 'NoteController@getNoteFile')->name('ajaxGetNoteFile');
Route::delete('/notefile/{id}/delete', 'NoteController@deleteNoteFile')->name('deleteNoteFile');

Route::resource('/result', 'ResultController');
Route::get('/ajaxGetResultFile', 'ResultController@getResultFile')->name('ajaxGetResultFile');
Route::delete('/resultfile/{id}/delete', 'ResultController@deleteResultFile')->name('deleteResultFile');

Route::resource('/syllabi', 'SyllabusController');
Route::get('/ajaxGetSyllabusFile', 'SyllabusController@getSyllabusFile')->name('ajaxGetSyllabusFile');
Route::delete('/syllabusfile/{id}/delete', 'SyllabusController@deleteSyllabusFile')->name('deleteSyllabusFile');

Route::resource('/question', 'OldQuestionController');
Route::get('/ajaxGetOldQuestionFile', 'OldQuestionController@getOldQuestionFile')->name('ajaxGetOldQuestionFile');
Route::delete('/oldquestionfile/{id}/delete', 'OldQuestionController@deleteOldQuestionFile')->name('deleteOldQuestionFile');

Route::resource('/department', 'DepartmentController');
});